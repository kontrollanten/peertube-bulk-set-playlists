const fs = require('fs');
const FormData = require('form-data');
const csvParser = require('csv-parser');
const fetch = require('node-fetch');
require('dotenv').config();

let accountName;

function wait() {
  return new Promise(resolve => setTimeout(resolve, 3));
}

function callApi({ body, method = 'GET', headers = {}, path }) {
  return fetch(`${process.env.PEERTUBE_URL}/api/v1${path}`, {
    body,
    headers: {
      'Authorization': `Bearer ${process.env.PEERTUBE_TOKEN}`,
      'content-type': 'application/json',
      ...headers,
    },
    method,
  })
    .then(async res => {
      if (res.status >= 300) {
        const text = await res.text();
        throw Error(`Failed with status ${res.status}: ${text}`);
      }
      return res.text();
    })
    .then(text => {
      return text ? JSON.parse(text) : {};
    });
}

async function convertPlaylistsToIds(videosByPlaylists) {
  let playlists;

  try {
    playlists = await callApi({
      path: `/accounts/${accountName}/video-playlists?count=50`,
    }).then(res => res.data);
  } catch (error) {
    console.error(`Failed to fetch playlists`, { error });
    throw Error();
  }

  if (!playlists) {
    throw Error(`Playlist response is empty`);
  }

  console.log(`Found ${playlists.length} playlists.`);

  return Object.keys(videosByPlaylists)
    .filter(pn => !!pn)
    .reduce(async (acc, playlistName) => {
      let { id: playlistId } = playlists.find(p => {
        return p.displayName.toLowerCase() === playlistName.toLowerCase()
      }) || {};

      if (!playlistId) {
        await wait();

        const body = new FormData();
        body.append('displayName', playlistName);
        body.append('privacy', 3);
        playlistId = await callApi({
          path: '/video-playlists',
          method: 'POST',
          body,
          headers: body.getHeaders(),
        })
          .then(res => {
            if (res.errors) {
              console.error(`Playlist name: ${playlistName}`);
              throw Error(Object.keys(res.errors).map(key => `${key}: ${JSON.stringify(res.errors[key])}`).join(', '));
            }
            return res.videoPlaylist.id;
          });
      }

      return {
        ...await acc,
        [playlistId]: videosByPlaylists[playlistName],
      };
    }, {});
}

const getVideoId = (() => {
  const cache = {};

  return async uuid => {
    if (cache[uuid]) return cache[uuid];

    await wait();
    const { id } = await callApi({ path: `/videos/${uuid}` });
    cache[uuid] = id;

    return id;
  };
})();

function mapVideosToPlaylists(videos) {
  return videos
    .reduce(async (playlists, video) => {
      playlists = await playlists
      const uuid = video[''].split('/').pop();

      process.stdout.write('.');

      const videoId = await getVideoId(uuid);

      Object.keys(video).
        forEach(playlist => {
          if (video[playlist].length === 0) return;

          playlists[playlist] = playlists[playlist] || [];
          playlists[playlist].push(videoId);
        });

      return playlists;
    }, {});
}

async function run() {
  const results = [];
  const me = await callApi({ path: '/users/me' });
  accountName = me.account.name;

  if (!accountName) {
    throw Error('Failed to retrieve account name.');
  }

  fs.createReadStream('videos.csv')
    .pipe(csvParser())
    .on('data', data => results.push(data))
    .on('end', async () => {
      console.log(`Found ${results.length} videos and ${Object.keys(results[0]).length} playlists.`);

      const videosByPlaylists = await convertPlaylistsToIds(await mapVideosToPlaylists(results));

      console.log(videosByPlaylists);

      Object.keys(videosByPlaylists)
        .forEach(playlistId => {
          videosByPlaylists[playlistId].forEach(async videoId => {
            const res = await callApi({
              path: `/video-playlists/${playlistId}/videos`,
              body: JSON.stringify({ videoId }),
              method: 'POST',
            });

            await wait();
            console.log({res});
          });
        });

      // Object.keys(results[0]).forEach(key => console.log(key));
    });
}

run();
